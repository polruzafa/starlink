import fs from 'fs'
import { execSync } from 'child_process'

const CTGSF = process.argv[2]
const CTS = process.argv[3]
let shipworlds = []
console.log(`The ship from ${CTGSF} will be sync'd with ${CTS}`)

fs.readdir('.', (err, files) => {
  if (err) {
    console.log(err)
    process.exit(2)
  }
  shipworlds = files.filter(file => {
    if (file.indexOf(CTGSF) !== -1 && file.indexOf('.player') === -1 && file.indexOf('zip') === -1) {
      return file
    }
  })

  let targets = shipworlds.map(file => `${CTS}${file.substr(file.indexOf('.'))}`)

  let commands = []
  shipworlds.forEach((file, index) => {
    commands.push(buildCommand(file, targets[index]))
  })

  commands.forEach(command => {
    execSync(command)
  })
})

const buildCommand = (from, target) => `mklink /H ${__dirname}\\${target} ${__dirname}\\${from}`
